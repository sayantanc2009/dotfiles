
# Cargo Path
PATH="$HOME/.cargo/bin${PATH:+:${PATH}}"

### EXPORT
export TERM="xterm-256color"                      # getting proper colors
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"

### Function EXTRACT for common file formats ###
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}

IFS=$SAVEIFS

## ALIASES ##
# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# git
#alias gaddup='git add -u'
#alias gadd='git add .'
#alias branch='git branch'
#alias checkout='git checkout'
#alias gclone='git clone'
#alias gcom='git commit -m'
#alias fetch='git fetch'
#alias pull='git pull origin'
#alias push='git push origin'
#alias gstat='git status'  # 'status' is protected name so using 'stat' instead
#alias tag='git tag'
#alias newtag='git tag -a'

# git bare
alias dot='/usr/bin/git --git-dir=$HOME/Documents/Git_Bare/dotfiles.git/ --work-tree=$HOME'
alias dotpush='dot push --set-upstream origin master'
alias other='/usr/bin/git --git-dir=$HOME/Documents/Git_Bare/others.git/ --work-tree=$HOME'
alias otherpush='other push --set-upstream others master'

# pacman and yay
alias ins='sudo pacman -S'			 # install standard pkgs
alias pacup='sudo pacman -Syyu'                  # update only standard pkgs
alias insy='yay -S'				 # install AUR (yay) pkgs
alias yayaur='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yayall='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'  # remove orphaned packages

# other useful aliases
alias clr='clear && zsh'
alias update-grub='sudo grub-mkconfig -o /boot/grub/grub.cfg'

# opening installed softwares
alias edit='sudo gedit'				# open Gedit as root

# switch between shells
# I do not recommend switching default SHELL from bash.
alias tobash="chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="chsh $USER -s /bin/fish && echo 'Now log out.'"
# End shell

# Powerline Shell Prompt
# function powerline_precmd() {
#    PS1="$(powerline-shell --shell zsh $?)"
# }
#
# function install_powerline_precmd() {
#  for s in "${precmd_functions[@]}"; do
#    if [ "$s" = "powerline_precmd" ]; then
#      return
#    fi
#  done
#  precmd_functions+=(powerline_precmd)
# }
#
# if [ "$TERM" != "linux" ]; then
#    install_powerline_precmd
# fi
# End prompt

# Starship Shell Prompt
eval "$(starship init zsh)"

neofetch
