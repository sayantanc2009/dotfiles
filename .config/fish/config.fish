
### ADDING TO THE PATH
# First line removes the path; second line sets it.  Without the first line,
# your path gets massive and fish becomes very slow.
set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/.cargo/bin $HOME/Applications $fish_user_paths

### EXPORT ###
set fish_greeting                                 # Supresses fish's intro message
set TERM "xterm-256color"                         # Sets the terminal type

### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

### END OF FUNCTIONS ###

## ALIASES ##
# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# git
alias gaddup='git add -u'
alias gadd='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias gclone='git clone'
alias gcom='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias gstat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'

# git bare
alias dot='/usr/bin/git --git-dir=$HOME/Documents/Git_Bare/dotfiles.git/ --work-tree=$HOME'
alias dotpush='dot push --set-upstream origin master'
alias other='/usr/bin/git --git-dir=$HOME/Documents/Git_Bare/others.git/ --work-tree=$HOME'
alias otherpush='other push --set-upstream others master'

# pacman and yay
alias ins='sudo pacman -S --needed'			 # install standard pkgs
alias pacup='sudo pacman -Syyu'                  # update only standard pkgs
alias insy='yay -S --needed'				 # install AUR (yay) pkgs
alias yayaur='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yayall='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'  # remove orphaned packages

# other useful aliases
alias clr='clear && fish'
alias update-grub='sudo grub-mkconfig -o /boot/grub/grub.cfg'

# opening installed softwares
alias edit='sudo gedit'				# open Gedit as root

# switch between shells
# I do not recommend switching default SHELL from bash.
alias tobash="chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="chsh $USER -s /bin/fish && echo 'Now log out.'"
# End shell

# Powerline Shell Prompt
# function fish_prompt
#    powerline-shell --shell bare $status
# end
# End prompt

# Starship Shell Prompt
#starship init fish | source

# Sashimi prompt
function fish_prompt
  set -l last_status $status
  set -l cyan (set_color -o cyan)
  set -l yellow (set_color -o yellow)
  set -g red (set_color -o red)
  set -g blue (set_color -o blue)
  set -l green (set_color -o green)
  set -g normal (set_color normal)

  set -l ahead (_git_ahead)
  set -g whitespace ' '

  if test $last_status = 0
    set initial_indicator "$green◆"
    set status_indicator "$normal❯$cyan❯$green❯"
  else
    set initial_indicator "$red✖ $last_status"
    set status_indicator "$red❯$red❯$red❯"
  end
  set -l cwd $cyan(basename (prompt_pwd))

  if [ (_git_branch_name) ]

    if test (_git_branch_name) = 'master'
      set -l git_branch (_git_branch_name)
      set git_info "$normal git:($red$git_branch$normal)"
    else
      set -l git_branch (_git_branch_name)
      set git_info "$normal git:($blue$git_branch$normal)"
    end

    if [ (_is_git_dirty) ]
      set -l dirty "$yellow ✗"
      set git_info "$git_info$dirty"
    end
  end

  # Notify if a command took more than 5 minutes
  if [ "$CMD_DURATION" -gt 300000 ]
    echo The last command took (math "$CMD_DURATION/1000") seconds.
  end

  echo -n -s $initial_indicator $whitespace $cwd $git_info $whitespace $ahead $status_indicator $whitespace
end

function _git_ahead
  set -l commits (command git rev-list --left-right '@{upstream}...HEAD' 2>/dev/null)
  if [ $status != 0 ]
    return
  end
  set -l behind (count (for arg in $commits; echo $arg; end | grep '^<'))
  set -l ahead  (count (for arg in $commits; echo $arg; end | grep -v '^<'))
  switch "$ahead $behind"
    case ''     # no upstream
    case '0 0'  # equal to upstream
      return
    case '* 0'  # ahead of upstream
      echo "$blue↑$normal_c$ahead$whitespace"
    case '0 *'  # behind upstream
      echo "$red↓$normal_c$behind$whitespace"
    case '*'    # diverged from upstream
      echo "$blue↑$normal$ahead $red↓$normal_c$behind$whitespace"
  end
end

function _git_branch_name
  echo (command git symbolic-ref HEAD 2>/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
  echo (command git status -s --ignore-submodules=dirty 2>/dev/null)
end
#End Sashimi

# Bira Prompt
# Adds a badge if we're in an SSH session (first letter of hostname, uppercased)
#function __ssh_badge
#	if test -n "$SSH_CLIENT$SSH2_CLIENT$SSH_TTY"
#		set_color -b d6aeec -o 2a0a8b
#		echo -n " "(string upper (string sub -s 1 -l 1 (hostname -s)))" "
#		set_color normal
#	end
#end

## only display a host name if we're in an ssh session
#function __ssh_host
#	if test -n "$SSH_CLIENT$SSH2_CLIENT$SSH_TTY"
#		set_color -d white
#		echo -n $USER@
#		set_color normal
#		set_color -d -o brmagenta
#		echo -n (hostname -s)
#		set_color normal
#	end
#end

#function __user_host
#	if test (id -u) -eq 0
#		set_color --bold red
#	else
#		set_color --bold green
#	end
#	echo -n $USER@(hostname -s) (set_color normal)
#end

#function __current_path
#	# Replace HOME with ~
#	set -l path (string replace "$HOME" (set_color a86ec8)"~"(set_color -d white) (pwd))
#	# Highlight last path element
#	set -l parts (string split "/" $path)
#	set parts[-1] (set_color normal)(set_color -o brwhite)$parts[-1](set_color normal)
#	set path (string join "/" $parts)

#	echo -n " "$path(set_color normal)
#end

#function _git_branch_name
#	echo (command git symbolic-ref HEAD 2> /dev/null | sed -e 's|^refs/heads/||')
#end

#function _git_is_dirty
#	echo (command git status -s --ignore-submodules=dirty 2> /dev/null)
#end

#function __git_status
#	if [ (_git_branch_name) ]
#		set -l git_branch (_git_branch_name)

#		if [ (_git_is_dirty) ]
#			set git_color "yellow"
#			set git_info '('$git_branch"*"')'
#		else
#			set git_color "green"
#			set git_info '('$git_branch')'
#		end

#		echo -n (set_color $git_color) $git_info (set_color normal)
#	end
#end

#function __ruby_version
#	if type "rvm-prompt" > /dev/null 2>&1
#		set ruby_version (rvm-prompt v g)
#	else if type "rbenv" > /dev/null 2>&1
#		set ruby_version (rbenv version-name)
#	else
#		set ruby_version "system"
#	end

#	echo -n (set_color red) "<$ruby_version>"(set_color normal)
#end

#function fish_prompt
#	set -l st $status
#	set -l pchar (set_color --bold white)"❯"
#	if [ $st != 0 ];
#		set pchar (set_color --bold red)"❯"
#	end

#	echo -n (set_color blue)"╭─"(set_color normal)
#	# __user_host
#	__ssh_badge
#	__current_path
#	__ruby_version
#	__git_status
#	echo -e ''

#	## SetMark adds iTerm marks, but now it's being set with an event hook
#	## via shell integration
#	# echo -e "\033]1337;SetMark\a"
#	echo -e (set_color blue)"╰─$pchar "(set_color normal)
#end

#function fish_right_prompt
#	set -l st $status
#	if [ $st != 0 ];
#		echo (set_color red) ↵ $st (set_color normal)
#	end
#	__ssh_host
#	set_color -o 666
#	date '+ %T'
#	set_color normal
#end

neofetch
